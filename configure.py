def main() -> None:
    IP = input("Enter an Arbitrary IP for your DDNS (To have internet connection the IP must be in your VMnet virtual bridge network e.g. 192.168.14.0 -> 192.168.14.*):")
    DOMAIN = input("Enter your domain of choice (e.g. example.com)")


if __name__ == "__main__":
    main()
